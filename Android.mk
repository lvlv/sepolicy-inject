LOCAL_PATH:= $(call my-dir)

common_src_files := \
	sepolicy-inject.c

common_cflags := \
	-Wall -Wshadow -O2 \
	-pipe -fno-strict-aliasing

ifeq ($(HOST_OS),darwin)
common_cflags += -DDARWIN
endif

common_includes := \
	$(LOCAL_PATH)/ \
	$(LOCAL_PATH)/../libsepol/include/ \
	$(LOCAL_PATH)/../libsepol/src/ \

##
# sepolicy-inject
#
include $(CLEAR_VARS)

LOCAL_MODULE := sepolicy-inject
LOCAL_MODULE_TAGS := optional
LOCAL_C_INCLUDES := $(common_includes)
LOCAL_CFLAGS := $(common_cflags)
LOCAL_SRC_FILES := $(common_src_files)
LOCAL_STATIC_LIBRARIES := libsepol
LOCAL_MODULE_CLASS := EXECUTABLES

include $(BUILD_HOST_EXECUTABLE)

